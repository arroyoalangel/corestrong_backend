export class QueryParamsDTO {
    order: 'ASC' | 'DESC' = 'ASC';
    orderKey = 'id';
    include = '';
    page = 1;
    limit = 10;

    get skip(): number {
        return (this.page - 1) * this.limit;
    }
    constructor(data?: Partial<QueryParamsDTO>) { }
}

export default QueryParamsDTO
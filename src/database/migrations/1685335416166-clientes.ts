import { MigrationInterface, QueryRunner } from "typeorm";

export class Clientes1685335416166 implements MigrationInterface {
    name = 'Clientes1685335416166'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "operaciones"."comprobantes" ("id" SERIAL NOT NULL, "fecha" date NOT NULL, "id_remitente" bigint NOT NULL, "id_destinatario" bigint NOT NULL, "tipo_cambio" numeric(10,2) NOT NULL, "monto_mn" numeric(10,2) NOT NULL, "monto_me" numeric(10,2) NOT NULL, CONSTRAINT "PK_0c3ac75b725717ec0f082ece89b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "operaciones"."clientes" ("id" BIGSERIAL NOT NULL, "nombre" character varying(20) NOT NULL, "apellidos" character varying(30) NOT NULL, "celular" character varying(20) NOT NULL, CONSTRAINT "PK_d76bf3571d906e4e86470482c08" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "operaciones"."comprobantes" ADD CONSTRAINT "FK_2a40dd0dae2f41c2f92f1280c1d" FOREIGN KEY ("id_remitente") REFERENCES "operaciones"."clientes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "operaciones"."comprobantes" ADD CONSTRAINT "FK_49eb7e85c4822fddf5494e0ec37" FOREIGN KEY ("id_destinatario") REFERENCES "operaciones"."clientes"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "operaciones"."comprobantes" DROP CONSTRAINT "FK_49eb7e85c4822fddf5494e0ec37"`);
        await queryRunner.query(`ALTER TABLE "operaciones"."comprobantes" DROP CONSTRAINT "FK_2a40dd0dae2f41c2f92f1280c1d"`);
        await queryRunner.query(`DROP TABLE "operaciones"."clientes"`);
        await queryRunner.query(`DROP TABLE "operaciones"."comprobantes"`);
    }

}

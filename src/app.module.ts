import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientesModule } from './app/clientes/clientes.module';
import { ComprobantesModule } from './app/comprobantes/comprobantes.module';
import * as dotenv from 'dotenv';
import * as path from 'path';

dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: async () => ({
        type: 'postgres',
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        schema: process.env.DB_SCHEMA,
        entities: [path.join(__dirname, '**', '*.entity.{ts,js}')],
      }),
    }),
    ClientesModule,
    ComprobantesModule,
  ],
})
export class AppModule { }

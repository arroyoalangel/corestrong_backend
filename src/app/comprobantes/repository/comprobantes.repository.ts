import { Injectable } from "@nestjs/common";
import { DataSource, DeleteResult, UpdateResult } from 'typeorm'
import { Comprobante } from "../domain/comprobantes.entity";
import QueryParamsDTO from "src/common/domain/QueryParams.dto";
import { ComprobanteCreate, ComprobanteUpdate } from "../domain/comprobantes.dto";
import { Cliente } from "src/app/clientes/domain/clientes.entity";

@Injectable()
export class ComprobantesRepository {
    constructor(private dataSource: DataSource) { }

    async create(payload: ComprobanteCreate): Promise<Comprobante> {
        const comprobante = new Comprobante()
        comprobante.fecha = payload.fecha;
        comprobante.tipoCambio = payload.tipoCambio;
        comprobante.montoMN = payload.montoMN;
        comprobante.montoME = payload.montoME;
        comprobante.idRemitente = payload.remitente as number;
        comprobante.idDestinatario = payload.destinatario as number;
        const response = await this.dataSource
            .getRepository(Comprobante)
            .save(comprobante)

        return response
    }

    async findAll(params: QueryParamsDTO): Promise<[Comprobante[], number]> {
        const { orderKey, order, include, limit, page } = {
            ...new QueryParamsDTO(),
            ...params
        }
        const skip = (page - 1) * limit
        const query = this.dataSource
            .getRepository(Comprobante)
            .createQueryBuilder('comprobantes')
            .select([
                'comprobantes.id',
                'comprobantes.fecha',
                'comprobantes.idRemitente',
                'comprobantes.idDestinatario',
                'comprobantes.tipoCambio',
                'comprobantes.montoMN',
                'comprobantes.montoME',
            ])
            .orderBy(`comprobantes.${orderKey}`, order)
            .skip(skip)
            .take(limit)
        const data = await query.getMany()
        const count = await query.getCount()
        return [data, count]
    }

    async findOne(idComprobante: string): Promise<Comprobante> {
        const query = this.dataSource
            .getRepository(Comprobante)
            .createQueryBuilder('comprobantes')
            .select([
                'comprobantes.fecha',
                'comprobantes.idRemitente',
                'comprobantes.idDestinatario',
                'comprobantes.tipoCambio',
                'comprobantes.montoMN',
                'comprobantes.montoME',
            ])
            .andWhere(
                'comprobantes.id = :idComprobante',
                { idComprobante }
            );
        const response = await query.getOne();
        return response;
    }

    async update(id: string, payload: ComprobanteUpdate): Promise<UpdateResult> {

        let comprobante = new Comprobante()
        comprobante.fecha = payload.fecha
        comprobante.tipoCambio = payload.tipoCambio
        comprobante.montoMN = payload.montoMN
        comprobante.montoME = payload.montoME
        comprobante.idRemitente = payload.remitente as number
        comprobante.idDestinatario = payload.destinatario as number

        const response = await this.dataSource
            .getRepository(Comprobante)
            .update(id, comprobante)

        return response
    }

    async delete(id: string): Promise<DeleteResult> {
        const response = await this.dataSource
            .getRepository(Comprobante)
            .delete(id)

        return response
    }
}
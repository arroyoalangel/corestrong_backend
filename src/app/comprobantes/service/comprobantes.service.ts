import { Injectable, Inject } from '@nestjs/common';
import { ComprobantesRepository } from '../repository/comprobantes.repository';
import { Comprobante } from '../domain/comprobantes.entity';
import QueryParamsDTO from 'src/common/domain/QueryParams.dto';
import { DeleteResult, UpdateResult } from 'typeorm';
import { ComprobanteCreate, ComprobanteUpdate } from '../domain/comprobantes.dto';

@Injectable()
export class ComprobantesService {
    constructor(
        @Inject(ComprobantesRepository) private repository: ComprobantesRepository
    ) { }

    async crear(payload: ComprobanteCreate): Promise<Comprobante> {
        const response = await this.repository.create(payload)
        return response
    }

    async findAll(params: QueryParamsDTO): Promise<[Comprobante[], number]> {
        const response = await this.repository.findAll(params)
        return response
    }

    async findOne(idComprobante: string): Promise<Comprobante> {
        const response = await this.repository.findOne(idComprobante)
        return response
    }

    async update(id: string, payload: ComprobanteUpdate): Promise<UpdateResult> {
        const response = this.repository.update(id, payload)
        return response
    }

    async delete(id: string): Promise<DeleteResult> {
        const response = await this.repository.delete(id)
        return response
    }
}

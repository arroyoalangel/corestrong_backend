import {
    Controller,
    Get, Post, Patch, Delete,
    Query, Param, Body, Put,
} from '@nestjs/common';
import QueryParamsDTO from 'src/common/domain/QueryParams.dto';
import { ComprobantesService } from '../service/comprobantes.service';
import { Comprobante } from '../domain/comprobantes.entity';
import { ComprobanteCreate, ComprobanteUpdate } from '../domain/comprobantes.dto';

@Controller('Comprobantes')
export class ComprobantesController {
    constructor(
        private service: ComprobantesService
    ) { }

    @Post()
    async create(
        @Body() comprobante: ComprobanteCreate
    ): Promise<Comprobante> {
        const result = await this.service.crear(comprobante)
        return result
    }

    @Get()
    async findAll(
        @Query() queryParams: QueryParamsDTO,
    ): Promise<[Comprobante[], number]> {
        const result = await this.service.findAll(queryParams)
        return result
    }

    @Get(':id')
    async findOne(
        @Param('id') idComprobante: string,
    ): Promise<Comprobante> {
        const result = await this.service.findOne(idComprobante)
        return result
    }

    @Put(':id')
    async update(
        @Param('id') id: string,
        @Body() comprobante: ComprobanteUpdate
    ) {
        const result = this.service.update(id, comprobante)
        return result
    }

    @Delete(':id')
    async delete(
        @Param('id') idComprobante: string
    ) {
        const result = await this.service.delete(idComprobante)
        return result
    }
}

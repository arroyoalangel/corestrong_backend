import { TypeOrmModule } from '@nestjs/typeorm'
import { Module } from '@nestjs/common';
import { ComprobantesController } from './controller/comprobantes.controller';
import { ComprobantesService } from './service/comprobantes.service';
import { Comprobante } from './domain/comprobantes.entity';
import { Cliente } from '../clientes/domain/clientes.entity';
import { ComprobantesRepository } from './repository/comprobantes.repository';

@Module({
    controllers: [ComprobantesController],
    providers: [ComprobantesService, ComprobantesRepository],
    imports: [
        TypeOrmModule.forFeature([
            Comprobante,
            Cliente,
        ]),
    ],
})
export class ComprobantesModule { }

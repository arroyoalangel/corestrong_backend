import { Cliente } from "src/app/clientes/domain/clientes.entity"

export interface ComprobanteCreate {
    fecha: Date
    tipoCambio: number
    montoMN: number
    montoME: number
    remitente: number | Cliente
    destinatario: number | Cliente
}

export interface ComprobanteUpdate {
    id?: string
    fecha?: Date
    tipoCambio?: number
    montoMN?: number
    montoME?: number
    remitente?: number | Cliente
    destinatario?: number | Cliente
}
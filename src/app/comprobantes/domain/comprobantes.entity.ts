import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Cliente } from '../../clientes/domain/clientes.entity';

@Entity({
    name: 'comprobantes',
    schema: 'operaciones',
})
export class Comprobante {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'date' })
    fecha: Date;

    @Column({ name: 'id_remitente' })
    idRemitente: number;

    @Column({ name: 'id_destinatario' })
    idDestinatario: number;

    @Column({ type: 'decimal', precision: 10, scale: 2, name: 'tipo_cambio' })
    tipoCambio: number;

    @Column({ type: 'decimal', precision: 10, scale: 2, name: 'monto_mn' })
    montoMN: number;

    @Column({ type: 'decimal', precision: 10, scale: 2, name: 'monto_me' })
    montoME: number;

    @ManyToOne(() => Cliente, (remitente) => remitente.comprobantesEnviados)
    @JoinColumn({
        name: 'id_remitente',
        referencedColumnName: 'id',
    })
    remitente: Cliente;

    @ManyToOne(() => Cliente, (destinatario) => destinatario.comprobantesRecibidos)
    @JoinColumn({
        name: 'id_destinatario',
        referencedColumnName: 'id',
    })
    destinatario: Cliente;

    constructor(data?: Partial<Comprobante>) { }
}

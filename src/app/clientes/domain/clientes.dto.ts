import { Comprobante } from "src/app/comprobantes/domain/comprobantes.entity"

export class ClienteCreate {
    nombre: string
    apellidos: string
    celular: string
    comprobantesEnviados?: Comprobante[] = []
    comprobantesRecibidos?: Comprobante[] = []
}

export class ClienteUpdate {
    id?: string
    nombre?: string
    apellidos?: string
    celular?: string
    comprobantesEnviados?: Comprobante[]
    comprobantesRecibidos?: Comprobante[]
}
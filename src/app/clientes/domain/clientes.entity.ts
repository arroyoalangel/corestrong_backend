import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Comprobante } from "../../comprobantes/domain/comprobantes.entity";

@Entity({
  name: 'clientes',
  schema: 'operaciones',
})
export class Cliente {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  id: string

  @Column({ length: 20, type: 'varchar' })
  nombre: string

  @Column({ length: 30, type: 'varchar' })
  apellidos: string

  @Column({ length: 20, type: 'varchar' })
  celular: string

  @OneToMany(() => Comprobante, comprobante => comprobante.remitente, { onDelete: 'SET NULL' })
  comprobantesEnviados?: Comprobante[];

  @OneToMany(() => Comprobante, comprobante => comprobante.destinatario, { onDelete: 'SET NULL' })
  comprobantesRecibidos?: Comprobante[];

  constructor(data?: Partial<Cliente>) { }
}

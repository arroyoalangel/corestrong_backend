import {
  Controller,
  Get, Post, Patch, Delete,
  Query, Param, Body, Put,
} from '@nestjs/common';
import { QueryParamsDTO } from '../../../common/domain/QueryParams.dto';
import { ClientesService } from '../service/clientes.service';
import { Cliente } from '../domain/clientes.entity';
import { ClienteCreate, ClienteUpdate } from '../domain/clientes.dto';

@Controller('clientes')
export class ClientesController {
  constructor(
    private service: ClientesService
  ) { }

  @Post()
  async create(
    @Body() cliente: ClienteCreate
  ): Promise<Cliente> {
    const result = await this.service.crear(cliente)
    return result
  }

  @Get()
  async findAll(
    @Query() QueryParams: QueryParamsDTO,
  ): Promise<[Cliente[], number]> {
    const result = await this.service.findAll(QueryParams)
    return result
  }

  @Get(':id')
  async findOne(
    @Param('id') idCliente: string,
  ): Promise<Cliente> {
    const result = await this.service.findOne(idCliente)
    return result
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() payload: ClienteUpdate
  ) {
    const result = this.service.update(id, payload)
    return result
  }

  @Delete(':id')
  async delete(
    @Param('id') id: string
  ) {
    const result = await this.service.delete(id)
    return result
  }
}

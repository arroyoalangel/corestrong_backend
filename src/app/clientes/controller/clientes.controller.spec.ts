import { Test } from '@nestjs/testing';
import { ClientesController } from './clientes.controller';
import { ClientesService } from '../service/clientes.service';
import { Cliente } from '../domain/clientes.entity';
import { ClientesRepository } from '../repository/clientes.repository';
import { QueryParamsDTO } from '../../../common/domain/QueryParams.dto';

describe('ClientesController', () => {
  let controller: ClientesController;
  let service: ClientesService;

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [ClientesController],
      providers: [ClientesService, ClientesRepository],
    }).compile();

    controller = moduleRef.get<ClientesController>(ClientesController);
    service = moduleRef.get<ClientesService>(ClientesService);
  });

  describe('create', () => {
    it('should create a new cliente', async () => {
      const cliente: Cliente = new Cliente();
      cliente.nombre = 'Pedro Jose'
      cliente.apellidos = 'Arroyo Sandoval'
      cliente.celular = '75214896'

      jest.spyOn(service, 'crear').mockResolvedValue(cliente);
      const result = await controller.create(cliente);

      expect(service.crear).toHaveBeenCalledWith(cliente);
      expect(result).toEqual(cliente);

    });
  });

  /*describe('findAll', () => {
    it('should return all clientes', async () => {
      const queryParams: QueryParamsDTO = new QueryParamsDTO();

      const clientes: Cliente[] = [];

      jest.spyOn(service, 'findAll').mockResolvedValue(clientes);

      const result = await controller.findAll(queryParams);

      expect(service.findAll).toHaveBeenCalledWith(queryParams);
      expect(result).toEqual(clientes);
    });
  });

  describe('findOne', () => {
    it('should return a single cliente', async () => {
      const idCliente = '1';
      const cliente: Cliente = new Cliente();
      cliente.nombre = 'Pedro Jose'
      cliente.apellidos = 'Arroyo Sandoval'
      cliente.celular = '75214896'

      jest.spyOn(service, 'findOne').mockResolvedValue(cliente);

      const result = await controller.findOne(idCliente);

      expect(service.findOne).toHaveBeenCalledWith(idCliente);
      expect(result).toEqual(cliente);
    });
  });

  describe('update', () => {
    it('should update a cliente', async () => {
      const id = '1';
      const payload: Cliente = new Cliente();

      jest.spyOn(service, 'update').mockResolvedValue(undefined);

      const result = await controller.update(id, payload);

      expect(service.update).toHaveBeenCalledWith(id, payload);
      expect(result).toBeUndefined();
    });
  });

  describe('delete', () => {
    it('should delete a cliente', async () => {
      const id = '1';

      jest.spyOn(service, 'delete').mockResolvedValue(undefined);

      const result = await controller.delete(id);

      expect(service.delete).toHaveBeenCalledWith(id);
      expect(result).toBeUndefined();
    });
  });*/
});

import { Injectable, Inject } from '@nestjs/common';
import { ClientesRepository } from '../repository/clientes.repository';
import { Cliente } from '../domain/clientes.entity';
import QueryParamsDTO from 'src/common/domain/QueryParams.dto';
import { DeleteResult, UpdateResult } from 'typeorm';
import { ClienteCreate, ClienteUpdate } from '../domain/clientes.dto';

@Injectable()
export class ClientesService {
  constructor(
    @Inject(ClientesRepository) private repository: ClientesRepository
  ) { }

  async crear(payload: ClienteCreate): Promise<Cliente> {
    const response = await this.repository.create(payload)
    return response
  }

  async findAll(params: QueryParamsDTO): Promise<[Cliente[], number]> {
    const response = await this.repository.findAll(params)
    return response
  }

  async findOne(idCliente: string): Promise<Cliente> {
    const response = await this.repository.findOne(idCliente)
    return response
  }

  async update(id: string, payload: ClienteUpdate): Promise<UpdateResult> {
    const response = this.repository.update(id, payload)
    return response
  }

  async delete(id: string): Promise<DeleteResult> {
    const response = await this.repository.delete(id)
    return response
  }
}

import { Injectable } from "@nestjs/common";
import { DataSource, DeleteResult, UpdateResult } from 'typeorm'
import { Cliente } from "../domain/clientes.entity";
import { QueryParamsDTO } from "../../../common/domain/QueryParams.dto";
import { ClienteCreate, ClienteUpdate } from "../domain/clientes.dto";

@Injectable()
export class ClientesRepository {
  constructor(private dataSource: DataSource) { }

  async create(payload: ClienteCreate): Promise<Cliente> {
    const response = await this.dataSource
      .getRepository(Cliente)
      .save(payload)
    return response
  }

  async findAll(params: QueryParamsDTO): Promise<[Cliente[], number]> {
    const { orderKey, order, include, limit, page } = {
      ...new QueryParamsDTO(),
      ...params
    }
    const skip = (page - 1) * limit
    const query = this.dataSource
      .getRepository(Cliente)
      .createQueryBuilder('clientes')
      .select([
        'clientes.id',
        'clientes.nombre',
        'clientes.apellidos',
        'clientes.celular',
      ])
      .orderBy(`clientes.${orderKey}`, order)
      .skip(skip)
      .take(limit)
      .andWhere(
        '(clientes.nombre ilike :include or clientes.apellidos ilike :include or clientes.celular ilike :include)',
        { include: `%${include}%` }
      )
    const data = await query.getMany()
    const count = await query.getCount()
    return [data, count]
  }

  async findOne(idCliente: string): Promise<Cliente> {
    const query = this.dataSource
      .getRepository(Cliente)
      .createQueryBuilder('clientes')
      .select([
        'clientes.nombre',
        'clientes.apellidos',
        'clientes.celular',
      ])
      .andWhere(
        'clientes.id = :idCliente',
        { idCliente }
      );
    const response = await query.getOne();
    return response;
  }

  async update(id: string, payload: ClienteUpdate): Promise<UpdateResult> {
    const response = await this.dataSource
      .getRepository(Cliente)
      .update(id, payload)

    return response
  }

  async delete(id: string): Promise<DeleteResult> {
    const response = await this.dataSource
      .getRepository(Cliente)
      .delete(id)

    return response
  }
}
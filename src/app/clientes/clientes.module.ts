import { TypeOrmModule } from '@nestjs/typeorm'
import { Module } from '@nestjs/common';
import { ClientesController } from './controller/clientes.controller';
import { ClientesService } from './service/clientes.service';
import { ClientesRepository } from './repository/clientes.repository';
import { Cliente } from './domain/clientes.entity';
import { Comprobante } from '../comprobantes/domain/comprobantes.entity';

@Module({
    controllers: [ClientesController],
    providers: [ClientesService, ClientesRepository],
    imports: [
        TypeOrmModule.forFeature([
            Cliente,
            Comprobante,
        ]),
    ],
})
export class ClientesModule { }
